﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DbFirstBook
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BookService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BookService.svc or BookService.svc.cs at the Solution Explorer and start debugging.
    public class BookService : IBookService
    {
        public List<book> GetAllBook()
        {
            List<book> booklst = new List<book>();
            BookEntities DbBook = new BookEntities();
            var list = from b in DbBook.books select b;

            foreach(var item in list)
            {
                book book = new book();
                book.Id = item.Id;
                book.Title = item.Title;
                book.ISBN = item.ISBN;
                booklst.Add(book);
            }
            return booklst;
        }
    }
}
