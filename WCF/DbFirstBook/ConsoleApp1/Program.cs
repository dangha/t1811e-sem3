﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.ServiceReference1;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            BookServiceClient client = new BookServiceClient();
            List<book> books = client.GetAllBook().ToList();
            foreach(var u in books)
            {
                Console.WriteLine("ID {0} ", u.Id);
                Console.WriteLine("Title {0}", u.Title);
                Console.WriteLine("ISBN {0}", u.ISBN);
            }Console.ReadLine();
        }
    }
}
