﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DemoApi.Controllers
{
    public class ValuesController : ApiController
    {
        public class Student
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }
        // GET api/values
        public IEnumerable<Student> Get()
        {
            return new List<Student>
            {
                new Student { ID = 1, Name = "Ha" },
                new Student{ ID = 2,Name = "Nguyen"}
            };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
