﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class order_detailController : Controller
    {
        private AssignmentEntities db = new AssignmentEntities();

        // GET: order_detail
        public ActionResult Index()
        {
            var order_detail = db.order_detail.Include(o => o.order).Include(o => o.product);

            return View(order_detail.ToList());
        }

        // GET: order_detail/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_detail order_detail = db.order_detail.Find(id);
            if (order_detail == null)
            {
                return HttpNotFound();
            }
            return View(order_detail);
        }

        // GET: order_detail/Create
        public ActionResult Create()
        {
            ViewBag.order_id = new SelectList(db.orders, "id", "id");
            ViewBag.product_id = new SelectList(db.products, "id", "category_id");
            return View();
        }

        // POST: order_detail/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,product_id,order_id,amount,price,payment,created")] order_detail order_detail)
        {
            if (ModelState.IsValid)
            {
                var price = from b in db.products
                            where b.id == order_detail.product_id
                            select b;

                var orderDetail = new order_detail()
                {
                    id = order_detail.id,
                    product_id = order_detail.product_id,
                    order_id = order_detail.order_id,
                    amount = order_detail.amount,
                    price = (price.FirstOrDefault().price - price.FirstOrDefault().price * price.FirstOrDefault().discount/100)
                    * order_detail.amount,
                    payment = order_detail.payment,
                    created = order_detail.created

                };
                db.order_detail.Add(orderDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.order_id = new SelectList(db.orders, "id", "id", order_detail.order_id);
            ViewBag.product_id = new SelectList(db.products, "id", "category_id", order_detail.product_id);
            return View(order_detail);
        }

        // GET: order_detail/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_detail order_detail = db.order_detail.Find(id);
            if (order_detail == null)
            {
                return HttpNotFound();
            }
            ViewBag.order_id = new SelectList(db.orders, "id", "id", order_detail.order_id);
            ViewBag.product_id = new SelectList(db.products, "id", "category_id", order_detail.product_id);
            return View(order_detail);
        }

        // POST: order_detail/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,product_id,order_id,amount,price,payment,created")] order_detail order_detail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order_detail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.order_id = new SelectList(db.orders, "id", "id", order_detail.order_id);
            ViewBag.product_id = new SelectList(db.products, "id", "category_id", order_detail.product_id);
            return View(order_detail);
        }

        // GET: order_detail/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_detail order_detail = db.order_detail.Find(id);
            if (order_detail == null)
            {
                return HttpNotFound();
            }
            return View(order_detail);
        }

        // POST: order_detail/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            order_detail order_detail = db.order_detail.Find(id);
            db.order_detail.Remove(order_detail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
