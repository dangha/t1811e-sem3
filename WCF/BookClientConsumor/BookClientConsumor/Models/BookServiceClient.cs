﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookClientConsumor.ServiceReference1;

namespace BookClientConsumor.Models
{
    public class BookServiceClient
    {
        BookServiceClient client = new BookServiceClient();
        public List<Book> GetAllBook()
        {
            var list = client.GetAllBook().ToList();
            var rt = new List<Book>();
            list.ForEach(b => rt.Add(new Book()
            {
                BookId = b.BookId,
                ISBN = b.ISBN,
                Title = b.Title
            }));
            return rt;
        }
    }
}