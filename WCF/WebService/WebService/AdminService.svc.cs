﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AdminService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AdminService.svc or AdminService.svc.cs at the Solution Explorer and start debugging.
    public class AdminService : IAdminService
    {
        WebDataContext data = new WebDataContext();
        public bool AddAdmin(admin a)
        {
            try
            {
                data.admins.InsertOnSubmit(a);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAdmin(int id)
        {
            try
            {
                var admin = from a in data.admins where a.id == id select a;
                data.admins.DeleteOnSubmit(admin.FirstOrDefault());
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool EditAdmin(admin item)
        {
            try
            {
                var admin = from a in data.admins where a.id == item.id select a;
                data.admins.DeleteOnSubmit(admin.FirstOrDefault());
                data.admins.InsertOnSubmit(item);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<admin> GetAdmins()
        {
            try
            {
                return (from a in data.admins select a).ToList();

            }
            catch
            {
                return null;
            }
        }
    }
}
