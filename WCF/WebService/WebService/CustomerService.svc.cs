﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CustomerService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CustomerService.svc or CustomerService.svc.cs at the Solution Explorer and start debugging.
    public class CustomerService : ICustomerService
    {
        WebDataContext data = new WebDataContext();
        public bool AddCustomer(customer c)
        {
            try
            {
                data.customers.InsertOnSubmit(c);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool DeleteCustomer(int id)
        {
            try
            {
                var customer = from b in data.customers where b.id == id select b;
                data.customers.DeleteOnSubmit(customer.FirstOrDefault());
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool EditCustomer(customer item)
        {
            try
            {
                var customer = from b in data.customers where b.id == item.id select b;
                data.customers.DeleteOnSubmit(customer.FirstOrDefault());
                data.customers.InsertOnSubmit(item);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public List<customer> GetCustomers()
        {
            try
            {
                return (from b in data.customers select b).ToList();
            }
            catch { return null; }
        }
    }
}
