﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfService1
{
    [DataContract]
    public class Book
    {
        [DataMember]
        public int BookId {get;set;}
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ISBN { get; set; }
    }
    public interface IBookReponsitory
    {
        List<Book> GetAllBook();
        Book GetBookById(int id);
        Book AddNewBook(Book item);
        bool DeleteABook(int id);
        bool UpdateABook(Book item);
    }
    public class BookReponsitory : IBookReponsitory
    {
        private List<Book> books = new List<Book>();
        private int counter = 1;

        //Contructor
        public BookReponsitory()
        {
            AddNewBook(new Book() { Title = "C# Programming", ISBN = "111111" });
            AddNewBook(new Book() { Title = "Java Programming", ISBN = "222222" });
            AddNewBook(new Book() { Title = "PHP Programming", ISBN = "777777" });
        }
        public Book AddNewBook(Book item)
        {
            if(item == null)
            {
                throw new AggregateException("newBook");
            }
            item.BookId = counter++;
            books.Add(item);
            return item;
        }

        public bool DeleteABook(int id)
        {
            int idx = books.FindIndex(b => b.BookId == id);
            if(idx == -1)
            {
                return false;
            }
            books.RemoveAll(b => b.BookId == id);
            return true;

        }

        public List<Book> GetAllBook()
        {
            return books;
        }

        public Book GetBookById(int id)
        {
            return books.Find(b => b.BookId == id);
        }

        public bool UpdateABook(Book item)
        {
            int idx = books.FindIndex(b => b.BookId == item.BookId);
            if(idx == -1)
            {
                return false;
            }
            books.RemoveAt(idx);
            books.Add(item);
            return true;
        }
    }
}