﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BookService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BookService.svc or BookService.svc.cs at the Solution Explorer and start debugging.
    public class BookService : IBookService
    {
        static IBookReponsitory reponsitory = new BookReponsitory();
        public string AddBook(Book book)
        {
            Book newBook = reponsitory.AddNewBook(book);
            return "id = " + newBook.BookId;
        }

        public string DeleteBook(string id)
        {
            bool deleted = reponsitory.DeleteABook(int.Parse(id));
            if (deleted)
            {
                return "Delete success";
            }
            else 
            { 
                return "Delete false"; 
            }
        }

        public Book GetBook(string id)
        {
            return reponsitory.GetBookById(int.Parse(id));
        }

        public List<Book> GetBookList()
        {
            return reponsitory.GetAllBook();
        }

        public string UpdateBook(Book book)
        {
            bool updated = reponsitory.UpdateABook(book);
            if (updated)
            {
                return "Update succesfull";
            }
            else
            {
                return "unable to update book";
            }
        }
    }
}
