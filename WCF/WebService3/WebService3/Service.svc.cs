﻿using System.Collections.Generic;
using System.Linq;

namespace WebService3
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service.svc or Service.svc.cs at the Solution Explorer and start debugging.
    public class Service : IService
    {
        ServiceDataContext data = new ServiceDataContext();

        public bool AddOrder(order o)
        {
            try
            {
                var total = from d in data.order_details
                            group d by d.order_id into y
                            select new
                            {
                                id = y.Key,
                                total = y.Sum(d => d.price)
                            };

                var newOrder = new order() { 
                    id = o.id,
                    Customer_id = o.Customer_id,
                    total_price = total.Where(p => p.id == o.id).FirstOrDefault().total,
                    status = o.status

            };

                data.orders.InsertOnSubmit(newOrder);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }
        //order detail
        public bool AddOrderDetail(order_detail c)
        {
            try
            {
                //cach viết lambda
                //c.price = db.products.Where(x => x.id == c.product_id).FirstOrDefault().price * c.amount;
                var price = from b in data.products
                            where b.id == c.product_id
                            select b;
                
                var orderDetail = new order_detail() {
                    id = c.id,
                    product_id = c.product_id,
                    order_id = c.order_id,
                    amount = c.amount,
                    price = (price.FirstOrDefault().price - price.FirstOrDefault().price * price.FirstOrDefault().discount)
                    * c.amount,
                    payment = c.payment,
                    created = c.created

                };
                data.order_details.InsertOnSubmit(orderDetail);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool DeleteOrder(int id)
        {

            try
            {
                var order = from b in data.orders where b.id == id select b;
                data.orders.DeleteOnSubmit(order.FirstOrDefault());
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool EditOrder(order item)
        {
            try
            {
                var order = from b in data.orders where b.id == item.id select b;
                data.orders.DeleteOnSubmit(order.FirstOrDefault());
                data.orders.InsertOnSubmit(item);
                return true;
            }
            catch { return false; }
        }

        public List<order> GetOrder()
        {
            try
            {
                return (from b in data.orders select b).ToList();
            }
            catch { return null; }
        }


        public bool DeleteOrderDetail(int id)
        {
            try
            {
                var orderDetail = from b in data.order_details
                                  where b.id == id
                                  select b;
                data.order_details.DeleteOnSubmit(orderDetail.FirstOrDefault());
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool EditOrderDetail(order_detail item)
        {
            try
            {
                var orderDetail = from b in data.order_details where b.id == item.id select b;
                data.order_details.DeleteOnSubmit(orderDetail.FirstOrDefault());
                data.order_details.InsertOnSubmit(item);
                return true;
            }
            catch { return false; }
        }

        public List<order_detail> GetOrderDetail()
        {
            try
            {
                return (from b in data.order_details select b).ToList();
            }
            catch { return null; }
        }
        public bool AddProduct(product p)
        {
            try
            {
                data.products.InsertOnSubmit(p);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool DeleteProduct(string id)
        {
            try
            {
                var product = from b in data.products where b.id == id select b;
                data.products.DeleteOnSubmit(product.FirstOrDefault());
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool EditProduct(product item)
        {
            try
            {
                var product = from b in data.products where b.id == item.id select b;
                data.products.DeleteOnSubmit(product.FirstOrDefault());
                data.products.InsertOnSubmit(item);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public List<product> GetProduct()
        {
            try
            {
                return (from p in data.products select p).ToList();
            }
            catch { return null; }
        }
        public bool AddCategory(category c)
        {
            try
            {
                data.categories.InsertOnSubmit(c);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool DeleteCategory(string id)
        {
            try
            {
                var category = from b in data.categories where b.id == id select b;
                data.categories.DeleteOnSubmit(category.FirstOrDefault());
                data.SubmitChanges();
                return true;

            }
            catch { return false; }
        }

        public bool EditCategory(category item)
        {
            try
            {
                var category = from b in data.categories where b.id == item.id select b;
                data.categories.DeleteOnSubmit(category.FirstOrDefault());
                data.categories.InsertOnSubmit(item);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public List<category> GetCategory()
        {
            try
            {
                return (from b in data.categories select b).ToList();
            }
            catch { return null; }
        }
        public bool AddCustomer(customer c)
        {
            try
            {
                data.customers.InsertOnSubmit(c);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool DeleteCustomer(int id)
        {
            try
            {
                var customer = from b in data.customers where b.id == id select b;
                data.customers.DeleteOnSubmit(customer.FirstOrDefault());
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool EditCustomer(customer item)
        {
            try
            {
                var customer = from b in data.customers where b.id == item.id select b;
                data.customers.DeleteOnSubmit(customer.FirstOrDefault());
                data.customers.InsertOnSubmit(item);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public List<customer> GetCustomers()
        {
            try
            {
                return (from b in data.customers select b).ToList();
            }
            catch { return null; }
        }
        public bool AddAdmin(admin a)
        {
            try
            {
                data.admins.InsertOnSubmit(a);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAdmin(int id)
        {
            try
            {
                var admin = from a in data.admins where a.id == id select a;
                data.admins.DeleteOnSubmit(admin.FirstOrDefault());
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool EditAdmin(admin item)
        {
            try
            {
                var admin = from a in data.admins where a.id == item.id select a;
                data.admins.DeleteOnSubmit(admin.FirstOrDefault());
                data.admins.InsertOnSubmit(item);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<admin> GetAdmins()
        {
            try
            {
                return (from a in data.admins select a).ToList();

            }
            catch
            {
                return null;
            }
        }
    }
}
