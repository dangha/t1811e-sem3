﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls.Expressions;
using Client.Models;


namespace Client.Controllers
{
    public class EmployeeController : Controller
    {
        BookEntities dba = new BookEntities();
        // GET: Employee
        public ActionResult Index(int searchString)
        {
            var db = from s in dba.Employees select s;

            if (searchString != -1)
            {
                db = db.Where(s => s.Departmant == searchString);
            }
            return View(db);
        }
    }
}