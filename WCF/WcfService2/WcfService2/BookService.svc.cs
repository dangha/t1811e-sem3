﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService2
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BookService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BookService.svc or BookService.svc.cs at the Solution Explorer and start debugging.
    public class BookService : IBookService
    {
        BookDataContext data = new BookDataContext();

        public bool AddBook(Book b)
        {
            try
            {
                data.Books.InsertOnSubmit(b);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteBook(int id)
        {
            try
            {
                var book = from b in data.Books where b.Id == id select b;
                data.Books.DeleteOnSubmit(book.FirstOrDefault());
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool EditBook(Book item)
        {
            try
            {
                var book = from b in data.Books where b.Id == item.Id select b;
                data.Books.DeleteOnSubmit(book.FirstOrDefault());
                data.Books.InsertOnSubmit(item);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public List<Book> GetBooks()
        {
            try
            {
                return (from b in data.Books select b).ToList();
            }
            catch { return null; }
        }
    }
}
