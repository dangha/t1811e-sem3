﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AssignmentService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service.svc or Service.svc.cs at the Solution Explorer and start debugging.
    public class Service : IService
    {
        ServiceDataContext data = new ServiceDataContext();

        public bool AddOrder(order o)
        {
            try
            {
                var total = from d in data.order_details
                            group d by d.order_id into y
                            select new
                            {
                                id = y.Key,
                                total = y.Sum(d => d.price)
                            };

                var newOrder = new order()
                {
                    id = o.id,
                    Customer_id = o.Customer_id,
                    total_price = total.Where(p => p.id == o.id).FirstOrDefault().total,
                    status = o.status

                };

                data.orders.InsertOnSubmit(newOrder);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public bool AddOrderDetail(order_detail c)
        {
            try
            {
                //cach viết lambda
                //c.price = db.products.Where(x => x.id == c.product_id).FirstOrDefault().price * c.amount;
                var price = from b in data.products
                            where b.id == c.product_id
                            select b;

                var orderDetail = new order_detail()
                {
                    id = c.id,
                    product_id = c.product_id,
                    order_id = c.order_id,
                    amount = c.amount,
                    price = (price.FirstOrDefault().price - price.FirstOrDefault().price * price.FirstOrDefault().discount)
                    * c.amount,
                    payment = c.payment,
                    created = c.created

                };
                data.order_details.InsertOnSubmit(orderDetail);
                data.SubmitChanges();
                return true;
            }
            catch { return false; }
        }

        public List<admin> GetAdmins()
        {
            try
            {
                return (from a in data.admins select a).ToList();

            }
            catch
            {
                return null;
            }
        }

        public List<category> GetCategory()
        {
            try
            {
                return (from b in data.categories select b).ToList();
            }
            catch { return null; }
        }

        public List<customer> GetCustomers()
        {
            try
            {
                return (from b in data.customers select b).ToList();
            }
            catch { return null; }
        }

        public List<order> GetOrder()
        {
            try
            {
                return (from b in data.orders select b).ToList();
            }
            catch { return null; }
        }

        public List<order_detail> GetOrderDetail()
        {
            try
            {
                return (from b in data.order_details select b).ToList();
            }
            catch { return null; }
        }

        public List<product> GetProduct()
        {
            try
            {
                return (from p in data.products select p).ToList();
            }
            catch { return null; }
        }
    }
}
