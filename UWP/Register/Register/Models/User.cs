﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Register.Models
{
    public class User
    {
        public string Username;
        public string Password;
        public string Name;
        public string Address;
        public string Email;
    }
}
