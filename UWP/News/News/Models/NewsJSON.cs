﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using static News.Models.NewsJSON;

namespace News.Models
{
    public class NewsJSON
    {
        public class Context
        {
            public bool required { get; set; }
            public string @default { get; set; }
        }

        public class Args
        {
            public Context context { get; set; }
        }

        public class Endpoint
        {
            public List<string> methods { get; set; }
            public Args args { get; set; }
        }

        public class Links
        {
            public string self { get; set; }
        }

        public class __invalid_type__
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint> endpoints { get; set; }
            public Links _links { get; set; }
        }

        public class Namespace
        {
            public bool required { get; set; }
            public string @default { get; set; }
        }

        public class Context2
        {
            public bool required { get; set; }
            public string @default { get; set; }
        }

        public class Args2
        {
            public Namespace @namespace { get; set; }
            public Context2 context { get; set; }
        }

        public class Endpoint2
        {
            public List<string> methods { get; set; }
            public Args2 args { get; set; }
        }

        public class Links2
        {
            public string self { get; set; }
        }

        public class Oembed10
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint2> endpoints { get; set; }
            public Links2 _links { get; set; }
        }

        public class Url
        {
            public bool required { get; set; }
        }

        public class Format
        {
            public bool required { get; set; }
            public string @default { get; set; }
        }

        public class Maxwidth
        {
            public bool required { get; set; }
            public int @default { get; set; }
        }

        public class Args3
        {
            public Url url { get; set; }
            public Format format { get; set; }
            public Maxwidth maxwidth { get; set; }
        }

        public class Endpoint3
        {
            public List<string> methods { get; set; }
            public Args3 args { get; set; }
        }

        public class Links3
        {
            public string self { get; set; }
        }

        public class Oembed10Embed
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint3> endpoints { get; set; }
            public Links3 _links { get; set; }
        }

        public class Url2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Format2
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Maxwidth2
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Maxheight
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Discover
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args4
        {
            public Url2 url { get; set; }
            public Format2 format { get; set; }
            public Maxwidth2 maxwidth { get; set; }
            public Maxheight maxheight { get; set; }
            public Discover discover { get; set; }
        }

        public class Endpoint4
        {
            public List<string> methods { get; set; }
            public Args4 args { get; set; }
        }

        public class Links4
        {
            public string self { get; set; }
        }

        public class Oembed10Proxy
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint4> endpoints { get; set; }
            public Links4 _links { get; set; }
        }

        public class Namespace2
        {
            public bool required { get; set; }
            public string @default { get; set; }
        }

        public class Context3
        {
            public bool required { get; set; }
            public string @default { get; set; }
        }

        public class Args5
        {
            public Namespace2 @namespace { get; set; }
            public Context3 context { get; set; }
        }

        public class Endpoint5
        {
            public List<string> methods { get; set; }
            public Args5 args { get; set; }
        }

        public class Links5
        {
            public string self { get; set; }
        }

        public class WpV2
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint5> endpoints { get; set; }
            public Links5 _links { get; set; }
        }

        public class Context4
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Page
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PerPage
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Search
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class After
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items
        {
            public string type { get; set; }
        }

        public class Author
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items items { get; set; }
        }

        public class Items2
        {
            public string type { get; set; }
        }

        public class AuthorExclude
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items2 items { get; set; }
        }

        public class Before
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items3
        {
            public string type { get; set; }
        }

        public class Exclude
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items3 items { get; set; }
        }

        public class Items4
        {
            public string type { get; set; }
        }

        public class Include
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items4 items { get; set; }
        }

        public class Offset
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Order
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Orderby
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items5
        {
            public string type { get; set; }
        }

        public class Slug
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items5 items { get; set; }
        }

        public class Items6
        {
            public List<string> @enum { get; set; }
            public string type { get; set; }
        }

        public class Status
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items6 items { get; set; }
            public List<string> @enum { get; set; }
        }

        public class Items7
        {
            public string type { get; set; }
        }

        public class Categories
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items7 items { get; set; }
        }

        public class Items8
        {
            public string type { get; set; }
        }

        public class CategoriesExclude
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items8 items { get; set; }
        }

        public class Items9
        {
            public string type { get; set; }
        }

        public class Tags
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items9 items { get; set; }
        }

        public class Items10
        {
            public string type { get; set; }
        }

        public class TagsExclude
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items10 items { get; set; }
        }

        public class Sticky
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Date
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class DateGmt
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Password
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Title
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Content
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Excerpt
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class FeaturedMedia
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class CommentStatus
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PingStatus
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Format3
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Template
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args6
        {
            public Context4 context { get; set; }
            public Page page { get; set; }
            public PerPage per_page { get; set; }
            public Search search { get; set; }
            public After after { get; set; }
            public Author author { get; set; }
            public AuthorExclude author_exclude { get; set; }
            public Before before { get; set; }
            public Exclude exclude { get; set; }
            public Include include { get; set; }
            public Offset offset { get; set; }
            public Order order { get; set; }
            public Orderby orderby { get; set; }
            public Slug slug { get; set; }
            public Status status { get; set; }
            public Categories categories { get; set; }
            public CategoriesExclude categories_exclude { get; set; }
            public Tags tags { get; set; }
            public TagsExclude tags_exclude { get; set; }
            public Sticky sticky { get; set; }
            public Date date { get; set; }
            public DateGmt date_gmt { get; set; }
            public Password password { get; set; }
            public Title title { get; set; }
            public Content content { get; set; }
            public Excerpt excerpt { get; set; }
            public FeaturedMedia featured_media { get; set; }
            public CommentStatus comment_status { get; set; }
            public PingStatus ping_status { get; set; }
            public Format3 format { get; set; }
            public Meta meta { get; set; }
            public Template template { get; set; }
        }

        public class Endpoint6
        {
            public List<string> methods { get; set; }
            public Args6 args { get; set; }
        }

        public class Links6
        {
            public string self { get; set; }
        }

        public class WpV2Posts
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint6> endpoints { get; set; }
            public Links6 _links { get; set; }
        }

        public class Id
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context5
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Password2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Date2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class DateGmt2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Slug2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Status2
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Title2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Content2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Author2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Excerpt2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class FeaturedMedia2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class CommentStatus2
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PingStatus2
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Format4
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Sticky2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Template2
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items11
        {
            public string type { get; set; }
        }

        public class Categories2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items11 items { get; set; }
        }

        public class Items12
        {
            public string type { get; set; }
        }

        public class Tags2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items12 items { get; set; }
        }

        public class Force
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args7
        {
            public Id id { get; set; }
            public Context5 context { get; set; }
            public Password2 password { get; set; }
            public Date2 date { get; set; }
            public DateGmt2 date_gmt { get; set; }
            public Slug2 slug { get; set; }
            public Status2 status { get; set; }
            public Title2 title { get; set; }
            public Content2 content { get; set; }
            public Author2 author { get; set; }
            public Excerpt2 excerpt { get; set; }
            public FeaturedMedia2 featured_media { get; set; }
            public CommentStatus2 comment_status { get; set; }
            public PingStatus2 ping_status { get; set; }
            public Format4 format { get; set; }
            public Meta2 meta { get; set; }
            public Sticky2 sticky { get; set; }
            public Template2 template { get; set; }
            public Categories2 categories { get; set; }
            public Tags2 tags { get; set; }
            public Force force { get; set; }
        }

        public class Endpoint7
        {
            public List<string> methods { get; set; }
            public Args7 args { get; set; }
        }

        public class WpV2PostsPIdD
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint7> endpoints { get; set; }
        }

        public class Parent
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context6
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args8
        {
            public Parent parent { get; set; }
            public Context6 context { get; set; }
        }

        public class Endpoint8
        {
            public List<string> methods { get; set; }
            public Args8 args { get; set; }
        }

        public class WpV2PostsPParentDRevisions
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint8> endpoints { get; set; }
        }

        public class Parent2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Id2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context7
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Force2
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args9
        {
            public Parent2 parent { get; set; }
            public Id2 id { get; set; }
            public Context7 context { get; set; }
            public Force2 force { get; set; }
        }

        public class Endpoint9
        {
            public List<string> methods { get; set; }
            public Args9 args { get; set; }
        }

        public class WpV2PostsPParentDRevisionsPIdD
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint9> endpoints { get; set; }
        }

        public class Context8
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Page2
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PerPage2
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Search2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class After2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items13
        {
            public string type { get; set; }
        }

        public class Author3
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items13 items { get; set; }
        }

        public class Items14
        {
            public string type { get; set; }
        }

        public class AuthorExclude2
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items14 items { get; set; }
        }

        public class Before2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items15
        {
            public string type { get; set; }
        }

        public class Exclude2
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items15 items { get; set; }
        }

        public class Items16
        {
            public string type { get; set; }
        }

        public class Include2
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items16 items { get; set; }
        }

        public class MenuOrder
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Offset2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Order2
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Orderby2
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items17
        {
            public string type { get; set; }
        }

        public class Parent3
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items17 items { get; set; }
        }

        public class Items18
        {
            public string type { get; set; }
        }

        public class ParentExclude
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items18 items { get; set; }
        }

        public class Items19
        {
            public string type { get; set; }
        }

        public class Slug3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items19 items { get; set; }
        }

        public class Items20
        {
            public List<string> @enum { get; set; }
            public string type { get; set; }
        }

        public class Status3
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items20 items { get; set; }
            public List<string> @enum { get; set; }
        }

        public class Date3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class DateGmt3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Password3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Title3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Content3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Excerpt3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class FeaturedMedia3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class CommentStatus3
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PingStatus3
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Template3
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args10
        {
            public Context8 context { get; set; }
            public Page2 page { get; set; }
            public PerPage2 per_page { get; set; }
            public Search2 search { get; set; }
            public After2 after { get; set; }
            public Author3 author { get; set; }
            public AuthorExclude2 author_exclude { get; set; }
            public Before2 before { get; set; }
            public Exclude2 exclude { get; set; }
            public Include2 include { get; set; }
            public MenuOrder menu_order { get; set; }
            public Offset2 offset { get; set; }
            public Order2 order { get; set; }
            public Orderby2 orderby { get; set; }
            public Parent3 parent { get; set; }
            public ParentExclude parent_exclude { get; set; }
            public Slug3 slug { get; set; }
            public Status3 status { get; set; }
            public Date3 date { get; set; }
            public DateGmt3 date_gmt { get; set; }
            public Password3 password { get; set; }
            public Title3 title { get; set; }
            public Content3 content { get; set; }
            public Excerpt3 excerpt { get; set; }
            public FeaturedMedia3 featured_media { get; set; }
            public CommentStatus3 comment_status { get; set; }
            public PingStatus3 ping_status { get; set; }
            public Meta3 meta { get; set; }
            public Template3 template { get; set; }
        }

        public class Endpoint10
        {
            public List<string> methods { get; set; }
            public Args10 args { get; set; }
        }

        public class Links7
        {
            public string self { get; set; }
        }

        public class WpV2Pages
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint10> endpoints { get; set; }
            public Links7 _links { get; set; }
        }

        public class Id3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context9
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Password4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Date4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class DateGmt4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Slug4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Status4
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Parent4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Title4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Content4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Author4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Excerpt4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class FeaturedMedia4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class CommentStatus4
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PingStatus4
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class MenuOrder2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Template4
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Force3
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args11
        {
            public Id3 id { get; set; }
            public Context9 context { get; set; }
            public Password4 password { get; set; }
            public Date4 date { get; set; }
            public DateGmt4 date_gmt { get; set; }
            public Slug4 slug { get; set; }
            public Status4 status { get; set; }
            public Parent4 parent { get; set; }
            public Title4 title { get; set; }
            public Content4 content { get; set; }
            public Author4 author { get; set; }
            public Excerpt4 excerpt { get; set; }
            public FeaturedMedia4 featured_media { get; set; }
            public CommentStatus4 comment_status { get; set; }
            public PingStatus4 ping_status { get; set; }
            public MenuOrder2 menu_order { get; set; }
            public Meta4 meta { get; set; }
            public Template4 template { get; set; }
            public Force3 force { get; set; }
        }

        public class Endpoint11
        {
            public List<string> methods { get; set; }
            public Args11 args { get; set; }
        }

        public class WpV2PagesPIdD
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint11> endpoints { get; set; }
        }

        public class Parent5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context10
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args12
        {
            public Parent5 parent { get; set; }
            public Context10 context { get; set; }
        }

        public class Endpoint12
        {
            public List<string> methods { get; set; }
            public Args12 args { get; set; }
        }

        public class WpV2PagesPParentDRevisions
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint12> endpoints { get; set; }
        }

        public class Parent6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Id4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context11
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Force4
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args13
        {
            public Parent6 parent { get; set; }
            public Id4 id { get; set; }
            public Context11 context { get; set; }
            public Force4 force { get; set; }
        }

        public class Endpoint13
        {
            public List<string> methods { get; set; }
            public Args13 args { get; set; }
        }

        public class WpV2PagesPParentDRevisionsPIdD
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint13> endpoints { get; set; }
        }

        public class Context12
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Page3
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PerPage3
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Search3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class After3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items21
        {
            public string type { get; set; }
        }

        public class Author5
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items21 items { get; set; }
        }

        public class Items22
        {
            public string type { get; set; }
        }

        public class AuthorExclude3
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items22 items { get; set; }
        }

        public class Before3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items23
        {
            public string type { get; set; }
        }

        public class Exclude3
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items23 items { get; set; }
        }

        public class Items24
        {
            public string type { get; set; }
        }

        public class Include3
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items24 items { get; set; }
        }

        public class Offset3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Order3
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Orderby3
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items25
        {
            public string type { get; set; }
        }

        public class Parent7
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items25 items { get; set; }
        }

        public class Items26
        {
            public string type { get; set; }
        }

        public class ParentExclude2
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items26 items { get; set; }
        }

        public class Items27
        {
            public string type { get; set; }
        }

        public class Slug5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items27 items { get; set; }
        }

        public class Items28
        {
            public List<string> @enum { get; set; }
            public string type { get; set; }
        }

        public class Status5
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items28 items { get; set; }
            public List<string> @enum { get; set; }
        }

        public class MediaType
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class MimeType
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Date5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class DateGmt5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Title5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class CommentStatus5
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PingStatus5
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Template5
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AltText
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Caption
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Description
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Post
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args14
        {
            public Context12 context { get; set; }
            public Page3 page { get; set; }
            public PerPage3 per_page { get; set; }
            public Search3 search { get; set; }
            public After3 after { get; set; }
            public Author5 author { get; set; }
            public AuthorExclude3 author_exclude { get; set; }
            public Before3 before { get; set; }
            public Exclude3 exclude { get; set; }
            public Include3 include { get; set; }
            public Offset3 offset { get; set; }
            public Order3 order { get; set; }
            public Orderby3 orderby { get; set; }
            public Parent7 parent { get; set; }
            public ParentExclude2 parent_exclude { get; set; }
            public Slug5 slug { get; set; }
            public Status5 status { get; set; }
            public MediaType media_type { get; set; }
            public MimeType mime_type { get; set; }
            public Date5 date { get; set; }
            public DateGmt5 date_gmt { get; set; }
            public Title5 title { get; set; }
            public CommentStatus5 comment_status { get; set; }
            public PingStatus5 ping_status { get; set; }
            public Meta5 meta { get; set; }
            public Template5 template { get; set; }
            public AltText alt_text { get; set; }
            public Caption caption { get; set; }
            public Description description { get; set; }
            public Post post { get; set; }
        }

        public class Endpoint14
        {
            public List<string> methods { get; set; }
            public Args14 args { get; set; }
        }

        public class Links8
        {
            public string self { get; set; }
        }

        public class WpV2Media
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint14> endpoints { get; set; }
            public Links8 _links { get; set; }
        }

        public class Id5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context13
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Date6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class DateGmt6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Slug6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Status6
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Title6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Author6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class CommentStatus6
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PingStatus6
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Template6
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AltText2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Caption2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Description2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Post2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Force5
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args15
        {
            public Id5 id { get; set; }
            public Context13 context { get; set; }
            public Date6 date { get; set; }
            public DateGmt6 date_gmt { get; set; }
            public Slug6 slug { get; set; }
            public Status6 status { get; set; }
            public Title6 title { get; set; }
            public Author6 author { get; set; }
            public CommentStatus6 comment_status { get; set; }
            public PingStatus6 ping_status { get; set; }
            public Meta6 meta { get; set; }
            public Template6 template { get; set; }
            public AltText2 alt_text { get; set; }
            public Caption2 caption { get; set; }
            public Description2 description { get; set; }
            public Post2 post { get; set; }
            public Force5 force { get; set; }
        }

        public class Endpoint15
        {
            public List<string> methods { get; set; }
            public Args15 args { get; set; }
        }

        public class WpV2MediaPIdD
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint15> endpoints { get; set; }
        }

        public class Context14
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args16
        {
            public Context14 context { get; set; }
        }

        public class Endpoint16
        {
            public List<string> methods { get; set; }
            public Args16 args { get; set; }
        }

        public class Links9
        {
            public string self { get; set; }
        }

        public class WpV2Types
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint16> endpoints { get; set; }
            public Links9 _links { get; set; }
        }

        public class Type
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context15
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args17
        {
            public Type type { get; set; }
            public Context15 context { get; set; }
        }

        public class Endpoint17
        {
            public List<string> methods { get; set; }
            public Args17 args { get; set; }
        }

        public class WpV2TypesPTypeW
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint17> endpoints { get; set; }
        }

        public class Context16
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args18
        {
            public Context16 context { get; set; }
        }

        public class Endpoint18
        {
            public List<string> methods { get; set; }
            public Args18 args { get; set; }
        }

        public class Links10
        {
            public string self { get; set; }
        }

        public class WpV2Statuses
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint18> endpoints { get; set; }
            public Links10 _links { get; set; }
        }

        public class Status7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context17
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args19
        {
            public Status7 status { get; set; }
            public Context17 context { get; set; }
        }

        public class Endpoint19
        {
            public List<string> methods { get; set; }
            public Args19 args { get; set; }
        }

        public class WpV2StatusesPStatusW
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint19> endpoints { get; set; }
        }

        public class Context18
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Type2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args20
        {
            public Context18 context { get; set; }
            public Type2 type { get; set; }
        }

        public class Endpoint20
        {
            public List<string> methods { get; set; }
            public Args20 args { get; set; }
        }

        public class Links11
        {
            public string self { get; set; }
        }

        public class WpV2Taxonomies
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint20> endpoints { get; set; }
            public Links11 _links { get; set; }
        }

        public class Taxonomy
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context19
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args21
        {
            public Taxonomy taxonomy { get; set; }
            public Context19 context { get; set; }
        }

        public class Endpoint21
        {
            public List<string> methods { get; set; }
            public Args21 args { get; set; }
        }

        public class WpV2TaxonomiesPTaxonomyW
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint21> endpoints { get; set; }
        }

        public class Context20
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Page4
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PerPage4
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Search4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items29
        {
            public string type { get; set; }
        }

        public class Exclude4
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items29 items { get; set; }
        }

        public class Items30
        {
            public string type { get; set; }
        }

        public class Include4
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items30 items { get; set; }
        }

        public class Order4
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Orderby4
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class HideEmpty
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Parent8
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Post3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items31
        {
            public string type { get; set; }
        }

        public class Slug7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items31 items { get; set; }
        }

        public class Description3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Name
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args22
        {
            public Context20 context { get; set; }
            public Page4 page { get; set; }
            public PerPage4 per_page { get; set; }
            public Search4 search { get; set; }
            public Exclude4 exclude { get; set; }
            public Include4 include { get; set; }
            public Order4 order { get; set; }
            public Orderby4 orderby { get; set; }
            public HideEmpty hide_empty { get; set; }
            public Parent8 parent { get; set; }
            public Post3 post { get; set; }
            public Slug7 slug { get; set; }
            public Description3 description { get; set; }
            public Name name { get; set; }
            public Meta7 meta { get; set; }
        }

        public class Endpoint22
        {
            public List<string> methods { get; set; }
            public Args22 args { get; set; }
        }

        public class Links12
        {
            public string self { get; set; }
        }

        public class WpV2Categories
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint22> endpoints { get; set; }
            public Links12 _links { get; set; }
        }

        public class Id6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context21
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Description4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Name2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Slug8
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Parent9
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta8
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Force6
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args23
        {
            public Id6 id { get; set; }
            public Context21 context { get; set; }
            public Description4 description { get; set; }
            public Name2 name { get; set; }
            public Slug8 slug { get; set; }
            public Parent9 parent { get; set; }
            public Meta8 meta { get; set; }
            public Force6 force { get; set; }
        }

        public class Endpoint23
        {
            public List<string> methods { get; set; }
            public Args23 args { get; set; }
        }

        public class WpV2CategoriesPIdD
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint23> endpoints { get; set; }
        }

        public class Context22
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Page5
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PerPage5
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Search5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items32
        {
            public string type { get; set; }
        }

        public class Exclude5
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items32 items { get; set; }
        }

        public class Items33
        {
            public string type { get; set; }
        }

        public class Include5
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items33 items { get; set; }
        }

        public class Offset4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Order5
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Orderby5
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class HideEmpty2
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Post4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items34
        {
            public string type { get; set; }
        }

        public class Slug9
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items34 items { get; set; }
        }

        public class Description5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Name3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta9
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args24
        {
            public Context22 context { get; set; }
            public Page5 page { get; set; }
            public PerPage5 per_page { get; set; }
            public Search5 search { get; set; }
            public Exclude5 exclude { get; set; }
            public Include5 include { get; set; }
            public Offset4 offset { get; set; }
            public Order5 order { get; set; }
            public Orderby5 orderby { get; set; }
            public HideEmpty2 hide_empty { get; set; }
            public Post4 post { get; set; }
            public Slug9 slug { get; set; }
            public Description5 description { get; set; }
            public Name3 name { get; set; }
            public Meta9 meta { get; set; }
        }

        public class Endpoint24
        {
            public List<string> methods { get; set; }
            public Args24 args { get; set; }
        }

        public class Links13
        {
            public string self { get; set; }
        }

        public class WpV2Tags
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint24> endpoints { get; set; }
            public Links13 _links { get; set; }
        }

        public class Id7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context23
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Description6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Name4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Slug10
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta10
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Force7
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args25
        {
            public Id7 id { get; set; }
            public Context23 context { get; set; }
            public Description6 description { get; set; }
            public Name4 name { get; set; }
            public Slug10 slug { get; set; }
            public Meta10 meta { get; set; }
            public Force7 force { get; set; }
        }

        public class Endpoint25
        {
            public List<string> methods { get; set; }
            public Args25 args { get; set; }
        }

        public class WpV2TagsPIdD
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint25> endpoints { get; set; }
        }

        public class Context24
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Page6
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PerPage6
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Search6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items35
        {
            public string type { get; set; }
        }

        public class Exclude6
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items35 items { get; set; }
        }

        public class Items36
        {
            public string type { get; set; }
        }

        public class Include6
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items36 items { get; set; }
        }

        public class Offset5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Order6
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Orderby6
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items37
        {
            public string type { get; set; }
        }

        public class Slug11
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items37 items { get; set; }
        }

        public class Items38
        {
            public string type { get; set; }
        }

        public class Roles
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items38 items { get; set; }
        }

        public class Username
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Name5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class FirstName
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class LastName
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Email
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Url3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Description7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Locale
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Nickname
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Password5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta11
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args26
        {
            public Context24 context { get; set; }
            public Page6 page { get; set; }
            public PerPage6 per_page { get; set; }
            public Search6 search { get; set; }
            public Exclude6 exclude { get; set; }
            public Include6 include { get; set; }
            public Offset5 offset { get; set; }
            public Order6 order { get; set; }
            public Orderby6 orderby { get; set; }
            public Slug11 slug { get; set; }
            public Roles roles { get; set; }
            public Username username { get; set; }
            public Name5 name { get; set; }
            public FirstName first_name { get; set; }
            public LastName last_name { get; set; }
            public Email email { get; set; }
            public Url3 url { get; set; }
            public Description7 description { get; set; }
            public Locale locale { get; set; }
            public Nickname nickname { get; set; }
            public Password5 password { get; set; }
            public Meta11 meta { get; set; }
        }

        public class Endpoint26
        {
            public List<string> methods { get; set; }
            public Args26 args { get; set; }
        }

        public class Links14
        {
            public string self { get; set; }
        }

        public class WpV2Users
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint26> endpoints { get; set; }
            public Links14 _links { get; set; }
        }

        public class Id8
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context25
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Username2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Name6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class FirstName2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class LastName2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Email2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Url4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Description8
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Locale2
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Nickname2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Slug12
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items39
        {
            public string type { get; set; }
        }

        public class Roles2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items39 items { get; set; }
        }

        public class Password6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta12
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Force8
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Reassign
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args27
        {
            public Id8 id { get; set; }
            public Context25 context { get; set; }
            public Username2 username { get; set; }
            public Name6 name { get; set; }
            public FirstName2 first_name { get; set; }
            public LastName2 last_name { get; set; }
            public Email2 email { get; set; }
            public Url4 url { get; set; }
            public Description8 description { get; set; }
            public Locale2 locale { get; set; }
            public Nickname2 nickname { get; set; }
            public Slug12 slug { get; set; }
            public Roles2 roles { get; set; }
            public Password6 password { get; set; }
            public Meta12 meta { get; set; }
            public Force8 force { get; set; }
            public Reassign reassign { get; set; }
        }

        public class Endpoint27
        {
            public List<string> methods { get; set; }
            public Args27 args { get; set; }
        }

        public class WpV2UsersPIdD
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint27> endpoints { get; set; }
        }

        public class Context26
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Username3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Name7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class FirstName3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class LastName3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Email3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Url5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Description9
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Locale3
        {
            public bool required { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Nickname3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Slug13
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items40
        {
            public string type { get; set; }
        }

        public class Roles3
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items40 items { get; set; }
        }

        public class Password7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta13
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Force9
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Reassign2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args28
        {
            public Context26 context { get; set; }
            public Username3 username { get; set; }
            public Name7 name { get; set; }
            public FirstName3 first_name { get; set; }
            public LastName3 last_name { get; set; }
            public Email3 email { get; set; }
            public Url5 url { get; set; }
            public Description9 description { get; set; }
            public Locale3 locale { get; set; }
            public Nickname3 nickname { get; set; }
            public Slug13 slug { get; set; }
            public Roles3 roles { get; set; }
            public Password7 password { get; set; }
            public Meta13 meta { get; set; }
            public Force9 force { get; set; }
            public Reassign2 reassign { get; set; }
        }

        public class Endpoint28
        {
            public List<string> methods { get; set; }
            public Args28 args { get; set; }
        }

        public class Links15
        {
            public string self { get; set; }
        }

        public class WpV2UsersMe
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint28> endpoints { get; set; }
            public Links15 _links { get; set; }
        }

        public class Context27
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Page7
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class PerPage7
        {
            public bool required { get; set; }
            public int @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Search7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class After4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items41
        {
            public string type { get; set; }
        }

        public class Author7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items41 items { get; set; }
        }

        public class Items42
        {
            public string type { get; set; }
        }

        public class AuthorExclude4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items42 items { get; set; }
        }

        public class AuthorEmail
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Before4
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items43
        {
            public string type { get; set; }
        }

        public class Exclude7
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items43 items { get; set; }
        }

        public class Items44
        {
            public string type { get; set; }
        }

        public class Include7
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items44 items { get; set; }
        }

        public class Offset6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Order7
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Orderby7
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Items45
        {
            public string type { get; set; }
        }

        public class Parent10
        {
            public bool required { get; set; }
            public object @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items45 items { get; set; }
        }

        public class Items46
        {
            public string type { get; set; }
        }

        public class ParentExclude3
        {
            public bool required { get; set; }
            public List<object> @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items46 items { get; set; }
        }

        public class Items47
        {
            public string type { get; set; }
        }

        public class Post5
        {
            public bool required { get; set; }
            public object @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
            public Items47 items { get; set; }
        }

        public class Status8
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Type3
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Password8
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AuthorIp
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AuthorName
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AuthorUrl
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AuthorUserAgent
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Content5
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Date7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class DateGmt7
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta14
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args29
        {
            public Context27 context { get; set; }
            public Page7 page { get; set; }
            public PerPage7 per_page { get; set; }
            public Search7 search { get; set; }
            public After4 after { get; set; }
            public Author7 author { get; set; }
            public AuthorExclude4 author_exclude { get; set; }
            public AuthorEmail author_email { get; set; }
            public Before4 before { get; set; }
            public Exclude7 exclude { get; set; }
            public Include7 include { get; set; }
            public Offset6 offset { get; set; }
            public Order7 order { get; set; }
            public Orderby7 orderby { get; set; }
            public Parent10 parent { get; set; }
            public ParentExclude3 parent_exclude { get; set; }
            public Post5 post { get; set; }
            public Status8 status { get; set; }
            public Type3 type { get; set; }
            public Password8 password { get; set; }
            public AuthorIp author_ip { get; set; }
            public AuthorName author_name { get; set; }
            public AuthorUrl author_url { get; set; }
            public AuthorUserAgent author_user_agent { get; set; }
            public Content5 content { get; set; }
            public Date7 date { get; set; }
            public DateGmt7 date_gmt { get; set; }
            public Meta14 meta { get; set; }
        }

        public class Endpoint29
        {
            public List<string> methods { get; set; }
            public Args29 args { get; set; }
        }

        public class Links16
        {
            public string self { get; set; }
        }

        public class WpV2Comments
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint29> endpoints { get; set; }
            public Links16 _links { get; set; }
        }

        public class Id9
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Context28
        {
            public bool required { get; set; }
            public string @default { get; set; }
            public List<string> @enum { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Password9
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Author8
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AuthorEmail2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AuthorIp2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AuthorName2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AuthorUrl2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class AuthorUserAgent2
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Content6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Date8
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class DateGmt8
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Parent11
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Post6
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Status9
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Meta15
        {
            public bool required { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Force10
        {
            public bool required { get; set; }
            public bool @default { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Args30
        {
            public Id9 id { get; set; }
            public Context28 context { get; set; }
            public Password9 password { get; set; }
            public Author8 author { get; set; }
            public AuthorEmail2 author_email { get; set; }
            public AuthorIp2 author_ip { get; set; }
            public AuthorName2 author_name { get; set; }
            public AuthorUrl2 author_url { get; set; }
            public AuthorUserAgent2 author_user_agent { get; set; }
            public Content6 content { get; set; }
            public Date8 date { get; set; }
            public DateGmt8 date_gmt { get; set; }
            public Parent11 parent { get; set; }
            public Post6 post { get; set; }
            public Status9 status { get; set; }
            public Meta15 meta { get; set; }
            public Force10 force { get; set; }
        }

        public class Endpoint30
        {
            public List<string> methods { get; set; }
            public Args30 args { get; set; }
        }

        public class WpV2CommentsPIdD
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint30> endpoints { get; set; }
        }

        public class Endpoint31
        {
            public List<string> methods { get; set; }
            public object args { get; set; }
        }

        public class Links17
        {
            public string self { get; set; }
        }

        public class WpV2Settings
        {
            public string @namespace { get; set; }
            public List<string> methods { get; set; }
            public List<Endpoint31> endpoints { get; set; }
            public Links17 _links { get; set; }
        }

        public class Routes
        {
            public __invalid_type__ __invalid_name__/ { get; set; }
        public Oembed10 __invalid_name__/oembed/1.0 { get; set; }
    public Oembed10Embed __invalid_name__/oembed/1.0/embed { get; set; }
public Oembed10Proxy __invalid_name__/oembed/1.0/proxy { get; set; }
    public WpV2 __invalid_name__/wp/v2 { get; set; }
    public WpV2Posts __invalid_name__/wp/v2/posts { get; set; }
    public WpV2PostsPIdD __invalid_name__/wp/v2/posts/(? P<id>[\d]+) { get; set; }
    public WpV2PostsPParentDRevisions __invalid_name__/wp/v2/posts/(? P<parent>[\d]+)/revisions { get; set; }
    public WpV2PostsPParentDRevisionsPIdD __invalid_name__/wp/v2/posts/(? P<parent>[\d]+)/revisions/(? P<id>[\d]+) { get; set; }
    public WpV2Pages __invalid_name__/wp/v2/pages { get; set; }
    public WpV2PagesPIdD __invalid_name__/wp/v2/pages/(? P<id>[\d]+) { get; set; }
    public WpV2PagesPParentDRevisions __invalid_name__/wp/v2/pages/(? P<parent>[\d]+)/revisions { get; set; }
    public WpV2PagesPParentDRevisionsPIdD __invalid_name__/wp/v2/pages/(? P<parent>[\d]+)/revisions/(? P<id>[\d]+) { get; set; }
    public WpV2Media __invalid_name__/wp/v2/media { get; set; }
    public WpV2MediaPIdD __invalid_name__/wp/v2/media/(? P<id>[\d]+) { get; set; }
    public WpV2Types __invalid_name__/wp/v2/types { get; set; }
    public WpV2TypesPTypeW __invalid_name__/wp/v2/types/(? P<type>[\w-]+) { get; set; }
    public WpV2Statuses __invalid_name__/wp/v2/statuses { get; set; }
    public WpV2StatusesPStatusW __invalid_name__/wp/v2/statuses/(? P<status>[\w-]+) { get; set; }
    public WpV2Taxonomies __invalid_name__/wp/v2/taxonomies { get; set; }
    public WpV2TaxonomiesPTaxonomyW __invalid_name__/wp/v2/taxonomies/(? P<taxonomy>[\w-]+) { get; set; }
    public WpV2Categories __invalid_name__/wp/v2/categories { get; set; }
    public WpV2CategoriesPIdD __invalid_name__/wp/v2/categories/(? P<id>[\d]+) { get; set; }
    public WpV2Tags __invalid_name__/wp/v2/tags { get; set; }
    public WpV2TagsPIdD __invalid_name__/wp/v2/tags/(? P<id>[\d]+) { get; set; }
    public WpV2Users __invalid_name__/wp/v2/users { get; set; }
    public WpV2UsersPIdD __invalid_name__/wp/v2/users/(? P<id>[\d]+) { get; set; }
    public WpV2UsersMe __invalid_name__/wp/v2/users/me { get; set; }
    public WpV2Comments __invalid_name__/wp/v2/comments { get; set; }
    public WpV2CommentsPIdD __invalid_name__/wp/v2/comments/(? P<id>[\d]+) { get; set; }
    public WpV2Settings __invalid_name__/wp/v2/settings { get; set; }
}

public class Help
{
    public string href { get; set; }
}

public class Links18
{
    public List<Help> help { get; set; }
}

public class RootObject
{
    public string name { get; set; }
    public string description { get; set; }
    public string url { get; set; }
    public string home { get; set; }
    public string gmt_offset { get; set; }
    public string timezone_string { get; set; }
    public List<string> namespaces { get; set; }
    public List<object> authentication { get; set; }
    public Routes routes { get; set; }
    public Links18 _links { get; set; }
}

public async static Task<List<RootObject>> GetNews(string url)
        {
            var http = new HttpClient();
            var response = await http.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();

            var serializer = new DataContractJsonSerializer(typeof(List<RootObject>));

            var dataStream = new MemoryStream(Encoding.UTF8.GetBytes(result));

            var value = serializer.ReadObject(dataStream) as List<RootObject>;

            return value;
        }
    }
}
