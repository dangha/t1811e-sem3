﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Annoation.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        //[DisplayName("Employee Name")]
        //[Required(ErrorMessage = "Employee Name is required")]
        //[StringLength(250,MinimumLength =4)]
        public string Name { get; set; }
        public string Address { get; set; }
        //[Range(3000,100000)]
        public decimal Salary { get; set; }
        [Required]
        //[DataType(DataType.EmailAddress)]
        //[RegularExpression(@"[a-z0-9]._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}",ErrorMessage ="Please enter correct email address")]
        public string Email { get; set; }
    }
}