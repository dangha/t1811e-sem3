﻿namespace AssignmentASP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Products");
            AlterColumn("dbo.Products", "ProductID", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Products", "ProductID");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Products");
            AlterColumn("dbo.Products", "ProductID", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Products", "ProductID");
        }
    }
}
