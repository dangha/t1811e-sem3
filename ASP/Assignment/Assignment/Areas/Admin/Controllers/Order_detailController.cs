﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment.Areas.Admin.Data;

namespace Assignment.Areas.Admin.Controllers
{
    public class Order_detailController : Controller
    {
        private RestaurantEntities db = new RestaurantEntities();

        // GET: Admin/Order_detail
        public ActionResult Index()
        {
            var order_detail = db.Order_detail.Include(o => o.Bill).Include(o => o.Product);
            return View(order_detail.ToList());
        }

        // GET: Admin/Order_detail/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_detail order_detail = db.Order_detail.Find(id);
            if (order_detail == null)
            {
                return HttpNotFound();
            }
            return View(order_detail);
        }

        // GET: Admin/Order_detail/Create
        public ActionResult Create()
        {
            ViewBag.Id_Bill = new SelectList(db.Bills, "Id", "Id");
            ViewBag.Id_Product = new SelectList(db.Products, "Id", "Name");
            return View();
        }

        // POST: Admin/Order_detail/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Id_Product,Id_Bill,Price")] Order_detail order_detail)
        {
            if (ModelState.IsValid)
            {
                db.Order_detail.Add(order_detail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_Bill = new SelectList(db.Bills, "Id", "Id", order_detail.Id_Bill);
            ViewBag.Id_Product = new SelectList(db.Products, "Id", "Name", order_detail.Id_Product);
            return View(order_detail);
        }

        // GET: Admin/Order_detail/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_detail order_detail = db.Order_detail.Find(id);
            if (order_detail == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_Bill = new SelectList(db.Bills, "Id", "Id", order_detail.Id_Bill);
            ViewBag.Id_Product = new SelectList(db.Products, "Id", "Name", order_detail.Id_Product);
            return View(order_detail);
        }

        // POST: Admin/Order_detail/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Id_Product,Id_Bill,Price")] Order_detail order_detail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order_detail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_Bill = new SelectList(db.Bills, "Id", "Id", order_detail.Id_Bill);
            ViewBag.Id_Product = new SelectList(db.Products, "Id", "Name", order_detail.Id_Product);
            return View(order_detail);
        }

        // GET: Admin/Order_detail/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_detail order_detail = db.Order_detail.Find(id);
            if (order_detail == null)
            {
                return HttpNotFound();
            }
            return View(order_detail);
        }

        // POST: Admin/Order_detail/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order_detail order_detail = db.Order_detail.Find(id);
            db.Order_detail.Remove(order_detail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
