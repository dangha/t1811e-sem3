//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Assignment.Areas.Admin.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        public int Id { get; set; }
        public int Id_detail { get; set; }
        public double Total_Price { get; set; }
        public int Id_Employee { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    
        public virtual Employee Employee { get; set; }
        public virtual Order_detail Order_detail { get; set; }
    }
}
