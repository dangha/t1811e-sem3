﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StudentModel01.Models
{
    public class Department
    {
        public int DepartmentID { get; set; }
        [StringLength(50,MinimumLength =3)]
        public string Name { get; set; }
        [DataType(DataType.Currency)]
        [Column(TypeName ="money")]
        public decimal Budget { get; set; }
        public DateTime StrartDate { get; set; }
        public int? intructorID { get; set; }
        public virtual Instructor Administrator { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
    }
}