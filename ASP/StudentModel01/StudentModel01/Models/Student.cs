﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StudentModel01.Models
{
    public class Student
    {
        public int Id { get; set; }
        [Required]
        [StringLength(30)]
        [Display(Name ="Last Name")]
        public string LastName { get; set; }
        [Required]
        [StringLength(30,ErrorMessage ="Cannot be longer than 30 characters")]
        [Column("FirstName")]
        public string FirstMidlName { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:yyyy-MM-dd}",ApplyFormatInEditMode =true)]
        [Display(Name ="Enrollment Date")]
        public DateTime EnrollmentDate { get; set; }

        [Display(Name ="Full Name")]
        public string FullName
        {
            get { return LastName + "," + FirstMidlName; }
        }
        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}