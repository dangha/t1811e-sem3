﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StudentModel01.Models
{
    public class Instructor
    {
        //vua khoa chinh vua khoa ngoai 0.1
        //[Key]
        //[ForeignKey("Instructor")]
        public int ID { get; set; }
        [Required]
        [Display(Name ="Last Name")]
        [StringLength(50)]
        public string LastName { get; set; }
        [Required]
        [Display(Name ="First Name")]
        [StringLength(50,ErrorMessage ="Cannot be longer than 50 characters")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name ="Hire Date")]
        [DisplayFormat(DataFormatString ="0:yyyy-MM-dd",ApplyFormatInEditMode =true)]
        public DateTime HireDate { get; set; }
        [Display(Name ="Full Name")]
        public string FullName
        {
            get { return LastName + "," + FirstName; }
        }
        public virtual ICollection<Course> Courses { get; set; }
        public virtual OfficeAssignment OfficeAssignment { get; set; }


    }
}