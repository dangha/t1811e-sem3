﻿namespace StudentModel01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddnewOfficeAssigment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OfficeAssignments",
                c => new
                    {
                        InstructorID = c.Int(nullable: false),
                        Location = c.String(),
                    })
                .PrimaryKey(t => t.InstructorID)
                .ForeignKey("dbo.Instructors", t => t.InstructorID)
                .Index(t => t.InstructorID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OfficeAssignments", "InstructorID", "dbo.Instructors");
            DropIndex("dbo.OfficeAssignments", new[] { "InstructorID" });
            DropTable("dbo.OfficeAssignments");
        }
    }
}
