﻿// <auto-generated />
namespace StudentModel01.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class CreateNewIntity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateNewIntity));
        
        string IMigrationMetadata.Id
        {
            get { return "202003261400457_CreateNewIntity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
